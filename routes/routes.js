const UserController = require('../controllers/user.controller');
const ThreadController = require('../controllers/thread.controller');
const CommentController = require('../controllers/comment.controller');
const FriendController = require('../controllers/friend.controller');
const VoteController = require('../controllers/votes.controller');

module.exports = (app) => {
    //USER
    app.post('/api/user', UserController.create);
    app.put('/api/user/:id', UserController.edit);
    app.delete('/api/user/:id/:password', UserController.delete);
    app.get('/api/user', UserController.get);
    app.get('/api/user/:id', UserController.getbyId);

    //THREAD
    app.post('/api/thread', ThreadController.create);
    app.put('/api/thread/:id', ThreadController.edit);
    app.delete('/api/thread/:id', ThreadController.delete);
    app.get('/api/thread', ThreadController.get);
    app.get('/api/thread/:id', ThreadController.getbyId);

    //COMMENT
    // app.post('/api/comment/:id', CommentController.create);
    app.put('/api/comment/:id', CommentController.edit);
    app.delete('/api/comment/:commentId/:threadId', CommentController.delete);
    app.get('/api/comment', CommentController.get);
    app.get('/api/comment/:id', CommentController.getbyId);
    app.post('/api/threadReply/:id', CommentController.threadReply);
    app.post('/api/commentReply/:id', CommentController.commentReply);

    //FRIEND
    app.get('/api/friend/:id', FriendController.get);
    app.post('/api/friend', FriendController.create);
    app.delete('/api/friend', FriendController.delete);

    //votes
    app.put('/api/thread/:id/vote', VoteController.voteThread);
    app.put('/api/comment/:id/vote', VoteController.voteComment);

    app.all("*", function (req, res) {
        res.status(404);
        res.json({
            description: "Unknown endpoint"
        });
    });
};
