const mongoose = require('mongoose');
const Thread = require('./thread.model');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    // a user needs to have a userName
    userName: {
        type: String,
        required: [true, 'A user needs to have a userName.'],
        unique: [true, 'must be unique for each User']
    },

    // A user needs to have a password
    password: {
        type: String,
        required: [true, 'A user needs to have a password.']
    },

    active:{
        type: Number,
        default: 1
    }

});

UserSchema.plugin(uniqueValidator);

// create the user model
const User = mongoose.model('user', UserSchema);

// export the user model
module.exports = User;
