const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const VoteSchema = require('./votes.schema');
const User = require('./user.model');
const uniqueValidator = require('mongoose-unique-validator');

const ThreadSchema = new Schema({
    // a thread needs to have a title
    title: {
        type: String,
        required: [true, 'A thread needs to have a title.']
    },

    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A user needs to be attached to a thread.'],
        ref: 'user',
        autopopulate: true
    },

    // A thread needs to have a content
    content: {
        type: String,
        required: [true, 'A thread needs to have content.']
    },

    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comment',
        autopopulate: true
    }],

    votes: {
        type: [VoteSchema],
    }
},{
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

// ThreadSchema.virtual('rating').get(function () {
//     // if there are no reviews we give back a message
//     if(this.votes.length === 0) {
//         return "no votes";
//     } else {
//         // computes the average rating
//         let sum = 0;
//         for(let vote of this.votes) {
//             sum += vote.vote;
//         }
//         return sum;
//     }
// });

ThreadSchema.pre('remove', function(next) {

    const Comment = mongoose.model('comment');

    Comment.updateMany({}, {$pull: {'comments': {'user': this._id}}})
        .then(() => next());
});

ThreadSchema.plugin(uniqueValidator);
ThreadSchema.plugin(require('mongoose-autopopulate'));
// create the thread model
const Thread = mongoose.model('thread', ThreadSchema);

// export the thread model
module.exports = Thread;



