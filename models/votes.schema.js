const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VoteSchema = new Schema({

    vote: {
        type: Number,
        required: [true, 'A vote is required.'],
        validate: {
            validator: (vote) => {
                return Number.isInteger(vote) &&  vote >= -1   && vote <= 1;
            },
            message: 'A vote has to be either -1 or 1'
        }
    },

    user: {
        type: Schema.Types.ObjectId,
        required: [true, 'A user needs to be attached to a vote.'],
        ref: 'user'
    }
});

module.exports = VoteSchema;
