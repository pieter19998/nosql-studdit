const mongoose = require('mongoose');
const VoteSchema = require('./votes.schema');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const ReplySchema = new Schema({
        // comment needs to have text
        content: {
            type: String,
            required: [true, 'A comment needs to have a text.']
        },
        user: {type: Schema.Types.ObjectId, ref: 'user'},

        parent_id: [{
            type: Schema.Types.ObjectId
        }],
        votes: {
            type: [VoteSchema],
        }
    },
    {
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);

// CommentSchema.virtual('voteAmount').get(function () {
//     // if there are no votes give back a message
//     if (this.votes.length === 0) {
//         return "no votes";
//     } else {
//         //calculate votes
//         let total = 0;
//         for (let vote of this.votes) {
//             total += vote.votes;
//         }
//         return total;
//     }
// });
// export the comment model
module.exports = ReplySchema;
