const express = require('express');
const routes = require('./routes/routes');
const bodyParser = require('body-parser');

const app = express();


// mongoose.Promise = global.Promise;
// if (process.env.NODE_ENV !== 'test') {
//   console.log("reaches server");
//   mongoose.connect('mongodb://localhost:27017/studdit');
// }


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
routes(app);

app.use((err, req, res, next) => {
    res.status(422).send({error: err.message});
});

module.exports = app;
