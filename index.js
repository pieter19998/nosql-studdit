const app = require('./app');
const neo4j = require('neo4j-driver').v1;
const mongoose = require('mongoose');

app.listen(3000, () => {
    console.log('Running on port 3000');
});

// connect to the database
mongoose.connect('mongodb://localhost:27017/studdit', {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {
        console.log('MongoDB connection established');

        // fire the event that the app is ready to listen
        app.emit('databaseConnected')
    })
    .catch(err => {
        console.log('MongoDB connection failed');
        console.log(err)
    });

const driver = neo4j.driver(' bolt://localhost:7687', neo4j.auth.basic('neo4j', 'studdit'));
const session = driver.session();
