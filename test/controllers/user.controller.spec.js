const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const User = mongoose.model('user');

describe('User controller', () => {
    it('Post to /api/user creates a new user', (done) => {
        User.count().then(count => {
            request(app)
                .post('/api/user')
                .send({userName: "jan", password: "test"})
                .end(() => {
                    User.count().then(newCount => {
                        assert(count + 1 === newCount);
                        done();
                    });
                });
        });
    });

    it('Post to /api/user requires a username and password', (done) => {
        request(app)
            .post('/api/user')
            .send({})
            .end((err, res) => {
                assert(res.body.error);
                done();
            });
    });

    it('Post to /api/user cant create a new user with existing username', (done) => {
        User.count().then(count => {
            request(app)
                .post('/api/user')
                .send({userName: "jan", password: "test"})
                .end(() => {
                    request(app)
                        .post('/api/user')
                        .send({userName: "jan", password: "test"})
                        .end((err, res) => {
                            assert(res.body.error);
                            done();
                        });
                });
        });
    });

    it('GET to /api/user find multiple users', (done) => {
        User.count().then(count => {
            request(app)
                .post('/api/user')
                .send({userName: "jan", password: "test"})
                .end(() => {
                    request(app)
                        .post('/api/user')
                        .send({userName: "willem", password: "test"})
                        .end(() => {
                            User.count().then(newCount => {
                                assert(count + 2 === newCount);
                                done();
                            });
                        });
                });
        });
    });

    it('Put to /api/user/username can update his password', done => {
        const user = new User({userName: 'joe', password: 'password'});

        user.save().then(() => {
            request(app)
                .put('/api/user/joe')
                .send({password: 'password', newPassword: 'newPassword'})
                .end(() => {
                    User.findOne({userName: 'joe'})
                        .then(user => {
                            assert(user.password === "newPassword");
                            done();
                        });
                });
        });
    });

    it('Delete to /api/user/ can delete a record', done => {
        const user = new User({userName: "joe", password: "password"});

        user.save().then(() => {
            request(app)
                .delete('/api/user/joe/password')
                .send()
                .end(() => {
                    User.findOne({userName: "joe", active: 1}).then(result => {
                        assert( result === null);
                        done();
                    });
                });
        });
    });
});
