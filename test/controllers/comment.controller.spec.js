const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const Comment = mongoose.model('comment');
const User = mongoose.model('user');
const Thread = mongoose.model('thread');


describe('Comment controller', () => {
    xit('POST to /api/comment create comment', done => {
        const user = new User({
            userName: "John",
            password: "password"
        });

        const thread = new Thread({
            title: 'testThread',
            content: 'thread content',
            userName: user._id
        });

        Promise.all([user.save(), thread.save()])
            .then(() => {
                Comment.count().then(count => {
                    request(app)
                        .post(`/api/comment/${thread._id}`)
                        .send({user: user._id, content: "This is comment content"})
                        .end(() => {
                            Comment.count().then(newCount => {
                                assert(count + 1 === newCount);
                                done();
                            });
                        });
                });
            });
    });

    xit('DELETE to /api/comment delete comment', done => {
        const user = new User({
            userName: "John",
            password: "password"
        });

        const thread = new Thread({
            title: 'testThread',
            content: 'thread content',
            userName: user._id
        });

        const comment = new Comment({
            content: 'thread content',
            user: user._id
        });

        Promise.all([user.save(), thread.save(), comment.save()])
            .then(() => {
                request(app)
                    .delete(`/api/comment/${comment._id}/${thread._id}`)
                    .end(() => {
                        Comment.count().then(count => {
                            assert(count === 0);
                            done();
                        });
                    });
            })
    })
});
