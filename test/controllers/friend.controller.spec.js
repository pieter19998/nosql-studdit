const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const User = mongoose.model('user');

describe('Friend controller', () => {
   xit('Post to /api/friend creates a new friendship', (done) => {

        request(app)
            .post('/api/friend')
            .send({userName: "jan" , befriend: "Willem"})
            .end((err , result) => {
                // console.log(JSON.stringify(result));
                assert(result.success === "success");
                done();
            })
    });
});

describe('Friend controller', () => {
    xit('Post to /api/friend creates a new friendship', (done) => {

        request(app)
            .delete('/api/friend')
            .send({userName: "jan" , unfriend: "Willem"})
            .end((err , result) => {
                // console.log(JSON.stringify(result));
                assert(result.success === "success");
                done();
            })
    });
});

