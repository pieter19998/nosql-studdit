const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const Thread = mongoose.model('thread');
const User = mongoose.model('user');
const Comment = mongoose.model('comment');

describe('Thread controller', () => {


    it('Post to /api/thread creates a new thread', (done) => {

        const user = new User({
            userName: "Joe",
            password: "password"
        });

        user.save().then(() => {
            Thread.count().then(count => {
                request(app)
                    .post('/api/thread')
                    .send({title: "Thread1", content: "Dit is een Thread", user: user._id})
                    .end(() => {
                        Thread.count().then(newCount => {
                            assert(count + 1 === newCount);
                            done();
                        });
                    });
            });
        });
    });

    it('Post to /api/thread requires a title, content and username', (done) => {
        request(app)
            .post('/api/thread')
            .send({})
            .end((err, res) => {
                assert(res.body.error);
                done();
            });
    });

    it('delete a thread', done => {
        const user = new User({
            userName: "Joe",
            password: "password"
        });

        const thread = new Thread({
            title: "Thread1",
            content: "Dit is een Thread",
            user: user._id
        });

        const comment = new Comment({
            content: "comment",
            user: user._id
        });

        Promise.all([user.save(), thread.save(), comment.save()])
            .then(() => {
                request(app)
                    .delete(`/api/thread/${thread._id}`)
                    .end(() => {
                        Comment.count()
                            .then(count => {
                                assert(count === null);
                                done();
                            });
                    });
            });
    });
});
