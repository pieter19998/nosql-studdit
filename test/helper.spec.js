const mongoose = require('mongoose')
const neo4j = require('neo4j-driver').v1;

// open a connection to the test database (don't use production database!)
before(done => {
    mongoose.connect('mongodb://localhost/studdittest');
    mongoose.connection
        .once('open', () => done())
        .on('error', error => {
            console.warn('Warning', error);
        });
});

beforeEach((done) => {
  const {users, threads, comments} = mongoose.connection.collections;

  users.drop(() => {
    threads.drop(() => {
      comments.drop(()=> {
        done();
      })
    })
  })
});
