module.exports = {

    get(req, res, next) {
        const userName = req.params.id;
        let friends = [];
        session.run(
            'MATCH (a:User { userName: {userParam} })-[:FRIEND]->(b)\n' +
            'RETURN collect(b.userName)', {userParam: userName})
            .then((result) => {
                result.records.forEach((record) => {
                    friends.push({
                        friends: record._fields[0]
                    });
                });
                res.status(200).send(friends);
                session.close();
            })
            .catch(next)
    },

    create(req, res, next) {
        const friendData = req.body;
            session.run(
                `MERGE (a:User {userName:"${friendData.userName}"})
            MERGE (b:User {userName:"${friendData.befriend}"})
            MERGE (a)-[r:FRIEND]->(b)
            RETURN a,r,b`)
                .then((result) => {

                    session.run(`RETURN EXISTS( (:User {userName: "${friendData.userName}"})-[:FRIEND]-(:User {userName: "${friendData.befriend}"}))`)
                        .then((result) => {
                            if (result.records[0]._fields[0] === true) {
                                res.status(200).json({
                                    success: "success"
                                });
                                session.close();
                            } else {
                                res.status(400).json({
                                    error: "failed to create relationship"
                                });
                                session.close();
                            }
                        })
                    // res.status(200).json({
                    //     success: "success"
                    // });
                    // session.close();
                })
                .catch(next)
        // }
    },

    delete(req, res, next) {
        const userName = req.body;
        const unfriend = req.body;

        if (userName || unfriend === undefined){
            res.status(400).send({error:'befriend or userName undefined'})
        }
        session.run(
            `MATCH (a:User{ userName: "${userName.userName}"})-[r:FRIEND]->(b:User{userName: "${unfriend.unfriend}"})
            DELETE r`)
            .then((result) => {

                session.run(`RETURN EXISTS( (:User {userName: "${userName.userName}"})-[:FRIEND]-(:User {userName: "${unfriend.unfriend}"}))`)
                    .then((j) =>{
                        if (j.records[0]._fields[0] === false){
                            res.status(200).json({
                                success: "deleted friendship"
                            });
                            session.close();
                        }else {
                            res.status(400).json({
                                error: "failed to delete relationship"
                            });
                            session.close();
                        }
                    })
            })
            .catch(next)
    },
};
