const Thread = require('../models/thread.model');
const Comment = require('../models/comment.model');

module.exports = {

    get(req, res, next) {
        // Thread.find({}, 'title content userName votes').populate('userName' , 'userName').sort({rating: 1})
        Thread.find({}, 'title content userName votes').populate('user' , 'userName')
            .then(thread => res.send(thread))
            .catch(next);
    },

    getbyId(req, res, next) {
        const threadId = req.params.id;

        Thread.findById({_id: threadId})
            .populate({
                path: 'comments',
                autopopulate: {
                    path: 'comments',
                    model: 'comment',}
            })
            .populate({
                path: 'user',
                autopopulate: {
                    path: 'user',
                    model: 'user',
                }})
            .then(thread => res.send(thread))
            .catch(next);
    },

    create(req, res, next) {
        const threadData = req.body;

        Thread.create(threadData)
            .then(thread => res.send(thread))
            .catch(next)
    },

    edit(req, res, next) {
        const threadId = req.params.id;
        const threadData = req.body;

        Thread.findByIdAndUpdate({_id: threadId}, {content: threadData.content})
            .then(() => Thread.findById({_id: threadId}))
            .then(thread => res.send(thread))
            .catch(next);
    },


    delete(req, res, next) {
        const threadId = req.params.id;

        Thread.findByIdAndRemove({_id: threadId})
            .then(thread => res.status(204).send(thread))
            .catch(next);
    }

};
