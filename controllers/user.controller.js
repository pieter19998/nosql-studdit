const User = require('../models/user.model');

module.exports = {

    get(req, res, next) {
        User.find({active: 1})
            .then(user => res.send(user))
            .catch(next);
    },

    getbyId(req, res, next) {
        const userId = req.params.id;

        // User.findOne({_id: userId , active: 1})
        User.findOne({userName: userId, active: 1})
            .then(user => res.send(user))
            .catch(next);
    },

    create(req, res, next) {
        const userData = req.body;

        User.create(userData)
            .then(user => res.status(200).send(user))
            .catch(next)
    },

    edit(req, res, next) {
        const userName = req.params.id;
        const userData = req.body;

        User.findOneAndUpdate({
            userName: userName,
            password: userData.password,
            active: 1
        }, {password: userData.newPassword}, {new: true}, (err, doc) => {

            if (doc === null) {
                res.status(401).send({error: 'unauthorized'})
            } else {
                res.send(doc)
            }
        })
    },

    delete(req, res, next) {

        const password = req.params.password;
        const userName = req.params.id;

        console.log(password)
        console.log(userName)

        User.findOneAndUpdate({userName: userName, password: password , active : 1}, {active: 0}, {new: true}, (err, doc) =>{
            console.log(doc)
            if (doc === null) {
                // res.status(401).send({error: 'unauthorized or user doesnt exist'})
                res.sendStatus(401)
            } else {
                // res.send(doc.active)
                res.sendStatus(200)
            }
        }).catch(err => (res.send('user doesnt exist')))
    }
};
