const Comment = require('../models/comment.model');
const Thread = require('../models/thread.model');

module.exports = {

    get(req, res, next) {
        Comment.find({})
            .then(comment => res.send(comment))
            .catch(next);
    },


    getbyId(req, res, next) {
        const commentId = req.params.id;

        Comment.findById({_id: commentId})
            .then(comment => res.send(comment))
            .catch(next);
    },

    create(req, res, next) {
        const commentData = req.body;
        const threadId = req.params.id;

        // var newComment = new Comment({user: commentData[0].user, content: commentData[0].content, votes: commentData[0].votes})
        let newComment = new Comment(commentData);
        console.log(newComment);

        newComment.save()
            .then(() => {
                Thread.findByIdAndUpdate(threadId, {"$push": {comments: newComment._id}})
                    .then(comment => res.send(comment))
                    .catch(next)
            }).catch(next)
    },

    edit(req, res, next) {
        const commentId = req.params.id;
        const commentData = req.body;

        Comment.findByIdAndUpdate({_id: commentId}, commentData)
            .then(() => Comment.findById({_id: commentId}))
            .then(comment => res.send(comment))
            .catch(next);
    },

    delete(req, res, next) {
        const commentId = req.params.commentId;
        const threadId = req.params.threadId;
        console.log(commentId);
        console.log(threadId);

       Comment.findByIdAndRemove(commentId)
            .then(() => {
                Thread.findByIdAndUpdate(threadId, {"$pull": {comments: commentId}})
                    .then(comment => res.send("oof"))
                    .catch(next)
            }).catch(next)
    },

    threadReply (req, res, next) {
        const newReply = new Comment(req.body);
        const threadId = req.params.id;

        newReply.save()
            .then(() => {
                Thread.findByIdAndUpdate(threadId, {"$push": {replies: newReply._id}})
                    .then(comment => res.send(comment))
                    .catch((error) => res.status(402).send({error: "cant add reply to thread"}))
            })
            .catch((error) => res.status(400).send({error: error.errmsg}))
    },

    commentReply (req, res, next) {
        const newReply = new Comment(req.body);
        const replyId = req.params.id;
        newReply.save()
            .then(() => {
                Comment.findByIdAndUpdate(replyId, {"$push": {comments: newReply._id}})
                    .then(comment => res.send(comment))
                    .catch(() => res.status(402).send({error: "cant add reply to thread"}))
            })
            .catch((error) => res.status(400).send({error: error.errmsg}))
    }

    //     Comment.findByIdAndRemove({_id: commentId})
    //         .then(comment => res.status(204).send(comment))
    //         .catch(next);
    // }

};
