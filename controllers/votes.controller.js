const Thread = require('../models/thread.model');
const Comment = require('../models/comment.model');

module.exports = {

    voteThread(req, res, next) {
        const threadId = req.params.id;
        const voteData = req.body;

        const vote = {
            vote: voteData.vote,
            user: voteData.user
        };

        console.log(voteData.user);
        console.log(voteData.vote);

        // Thread.findByIdAndUpdate(threadId, {"$pullAll": {votes: {user: voteData.user}}},{runValidators: true})
        Thread.findByIdAndUpdate(threadId, { $pull: { votes: [{ user: voteData.vote}] }},{runValidators: true})
        // Thread.findByIdAndUpdate(threadId, {"$pull": {votes: vote}},{runValidators: true})
            .then( () =>{
                Thread.findByIdAndUpdate(threadId, {"$push": {votes: vote}},{runValidators: true})
                    .then((result) => {
                        res.send(result)
                    })
        })
            .catch(next)
    },

    voteComment(req, res, next) {
        const commentId = req.params.id;
        const voteData = req.body;

        const vote = {
            vote: voteData.vote,
            user: voteData.userId
        };

        console.log(voteData.userId);
        console.log(voteData.vote);

        Comment.findByIdAndUpdate({ _id: commentId }, {$push:  {votes: vote}}, {runValidators: true})
            .then(() => Comment.findById({ _id: commentId }))
            .then(comment => res.send(comment))
            .catch(next);
    }
};
